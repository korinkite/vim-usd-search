`vim-usd-search` is a plugin that's meant to search Pixar USD files for a
search term.

Users of USD know that USD files combine in non-obvious ways so it can
be hard to find code that you're interested in, as a result. To get
around this, most people will use `usdview` to inspect the composed USD
files for data. But what if you want to search the uncomposed contents
of files or you aren't sure what you're looking for? In either case,
`usdview` won't help you. For anyone that has run into those issues or
generally prefers to read code anyway, this plugin will help you!

This plugin works with file paths and any kind of recognized USD URI. As
long as a path can be resolved using USD's ArResolver, this plugin will
follow the path and search text in it.


## Requirements
- USD must be installed and sourced in your Vim environment. Otherwise,
URIs cannot resolve into resources on-disk
- The `usd_search` Python module must be in your PYTHONPATH


## Configuration Options


|               Variable                |                                                                           Description                                                                           |                  Default                  |
|---------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------|
| g:vim_usd_search_ignore_unresolved    | If set to `0` then raise an error if any computed file could not be found on-disk                                                                               | 1                                         |
| g:vim_usd_search_skip_binary          | If `0` then even binary files, such as .usdc files, will be searched for text                                                                                   | 1                                         |
