" Search the dependencies of `path` for `phrase`.
"
" This function will open matched results in a Quickfix window.
"
" Args:
"     phrase (str):
"         The term that will be searched for.
"     path (str):
"         The USD file path that will be used to check for dependencies.
"     allow_assets (bool):
"         If True then asset paths will be returned. False then only
"         If found USD layers will be returned.
"     ignore_unresolved (bool, optional):
"         If False and any unresolved paths are found,
"         `vim_usd_search#search` will error. If True then the function
"         will pass through the unresolved paths, silently.
"
function! vim_usd_search#search(phrase, path, allow_assets, ...)
    let l:ignore_unresolved = a:0 >= 2 ? a:2 : get(g:, 'vim_usd_search_ignore_unresolved', 1)

pythonx << EOF
import json

import usd_searcher
import vim

matches = usd_searcher.search(
    vim.eval("a:phrase"),
    vim.eval("a:path"),
    include_layers=True,
    include_assets=vim.eval("a:allow_assets"),
    ignore_unresolved=vim.eval("l:ignore_unresolved"),
)

data = []
for match in matches:
    entry = {"filename": match.path, "text": match.text, "lnum": match.row + 1}
    data.append(entry)

vim.command('call setqflist({data})'.format(data=data))
EOF

if !empty(getqflist())
    copen
endif
endfunction
