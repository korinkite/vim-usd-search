" Search for "foo" in a file
" :SearchFile foo %
"
command! -bang -nargs=+ SearchFile :call vim_usd_search#search(<q-args>, <bang>0)

" Search for "foo" in the current file
" :SearchCurrent foo
"
command! -bang -nargs=1 SearchCurrent :call vim_usd_search#search(<q-args>, expand('%:p'), <bang>0)
